Build a simple CRUD application using this API https://jsonplaceholder.typicode.com/. It should fetch data from 3 resources of the API, combine them and then display them as a list. The resources are

/posts  
/users  
/albums

The combined collection should contain only 30 items each of which should contain random items from each resource, finally looking something like this

`[{"post" : {...},"album" : {...}"user" : {...}},...]`

We do not care about the extra items remaining from each resource.This list should be rendered on the DOM in anyway you feel like and should display the following per list item: the post title, album title and user name. For each rendered item include a way to also delete the item and update the post title only.

You will have 24h to complete this test. If you finish earlier, make sure you test your code and go for the bonus points. Submit your test by sending us your github repo where you uploaded your code to.

### Some tips: 
* You can use ANY framework of your choice both for Javascript and for CSS but the code MUST be in Typescript
* It must come with a webpack build (so live dev server/build command). If your framework takes care of that, all the better
* You must provide clear instructions on how to setup and run the project
* Don't spend time on making it look good, rather spend time making it efficientBonus points for good documentation and a polished final look & feel
* Extra bonus points for TDD (not a requirement though)