Visit https://www.saucedemo.com/

### Login Success
* Login with `standard_user` 
* Test for Successful login

### Login Failure
* Login with `standard_user` 
* use wrong password
* Test for error message

### Login Locked out user
* Login with `locked_out_user` 
* Test for error message

### Add to cart
* Login with `standard_user`
* Add 3 items to cart
* Visit cart, test that items are there

### Checkout form
* Add items to cart
* Go to checkout and fill out the form
* Test for various states

### Checkout overview
* Add items to cart
* Go to checkout and fill out the form
* Click continue
* test that items are there
* Validate the total against the items in cart
* Click finish and validate for success/error state

### Bonus points
* Visit the product list under all items
* Sort by price high to low, and test out the validity of the sort
* Sort by name Z to A, and test out the validity of the sort
* Reset the app state and test that it is indeed reset