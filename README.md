# SilverHorseTech Recruitment Tests
All tests are of equal difficulty so just pick the one best suited to your skills.

# Available tests
[Front End](./front-end.md)

[Javascript Back End](./backend-js.md)

[.NET Back End](./backend-dot-net.md)

[Automation Test](./automation-testing.md)
